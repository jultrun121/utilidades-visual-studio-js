
//
const trueCondition = true;
const falseCondition = false;

const withoutParentheses = {
  ...trueCondition && { birds: "tweet" },
  ...falseCondition && { foxes: "???" },
};

//
const trueCondition = true;
const falseCondition = false;

const arr = [
  ...(trueCondition ? ["dog"] : []),
  ...(falseCondition ? ["cat"] : [])
];

<Component prop1={1} {...falseCondition && { foxes: "???" }}/>